#ifndef GUI_H
#define GUI_H

const char *gui_rootdir = "gui";
const unsigned gui_count = 1;

const struct {
	const bool isDir;
	const char *relativePath;
	const char *name;
	const unsigned size;
	const char *data;
} gui[1] = {
	{
		false,
		"gui/",
		"mainwindow.json",
		2974,
		"{\n" \
		"    \"name\" : \"window\",\n" \
		"    \"type\" : \"DoubleWindow\",\n" \
		"    \"title\" : \"Audio spectrum\",\n" \
		"    \"region\" : [0, 0, 800, 480],\n" \
		"    \"color\" : [0, 0, 0],\n" \
		"    \"layout\" : {\n" \
		"        \"type\" : \"HLayout\",\n" \
		"        \"spacing\" : 4,\n" \
		"        \"stretch\" : true,\n" \
		"        \"children\" : [\n" \
		"            {\n" \
		"                \"type\" : \"VLayout\",\n" \
		"                \"spacing\" : 0,\n" \
		"                \"stretch\" : true,\n" \
		"                \"children\" : [\n" \
		"                    {\n" \
		"                        \"name\" : \"fft\",\n" \
		"                        \"type\" : \"CurvesChart\",\n" \
		"                        \"color\" : [0, 0, 0],\n" \
		"                        \"bounds\" : [0.0, 25],\n" \
		"                        \"curve\" : \"StepSpike\"\n" \
		"                    }\n" \
		"                ]\n" \
		"            }\n" \
		"        ],\n" \
		"        \"footer\" : {\n" \
		"            \"height\" : 25,\n" \
		"            \"type\" : \"HLayout\",\n" \
		"            \"spacing\" : 4,\n" \
		"            \"stretch\" : true,\n" \
		"            \"box\" : \"FLAT_BOX\",\n" \
		"            \"color\" : [30, 30, 30],\n" \
		"            \"children\" : [\n" \
		"                {\n" \
		"                    \"name\" : \"fsButton\",\n" \
		"                    \"type\" : \"LightButton\",\n" \
		"                    \"color\" : [60, 60, 60],\n" \
		"                    \"size\" : [100, 0],\n" \
		"                    \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"label\" : \"Fullscreen\",\n" \
		"                    \"tooltip\" : \"Toggle fullscreen\",\n" \
		"                    \"align\" : \"ALIGN_CENTER\"\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"chansLabel\",\n" \
		"                    \"type\" : \"Box\",\n" \
		"                    \"color\" : [60, 60, 60],\n" \
		"                    \"size\" : [80, 0],\n" \
		"                    \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"label\" : \"Channels:\",\n" \
		"                    \"align\" : [\"ALIGN_RIGHT\", \"ALIGN_INSIDE\"]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"chansCombo\",\n" \
		"                    \"type\" : \"ComboBox\",\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"tooltip\" : \"Select the input channel\"\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"LayoutSpacer\"\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"quitButton\",\n" \
		"                    \"type\" : \"Button\",\n" \
		"                    \"size\" : [70, 0],\n" \
		"                    \"resizePolicy\" : \"FIXED_WIDTH\",\n" \
		"                    \"color\" : [60, 60, 60],\n" \
		"                    \"labelFontColor\" : [255, 255, 255],\n" \
		"                    \"labelFont\" : \"SCREEN_BOLD\",\n" \
		"                    \"labelFontSize\" : 12,\n" \
		"                    \"label\" : \"Quit\",\n" \
		"                    \"tooltip\" : \"Close application\",\n" \
		"                    \"align\" : \"ALIGN_CENTER\"\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    }\n" \
		"}\n"
	}
};

#define GUI_INDEX	"{\"gui/mainwindow.json\":0}"

#endif // GUI_H
