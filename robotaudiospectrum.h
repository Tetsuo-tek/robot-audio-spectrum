#ifndef ROBOTAUDIOSPECTRUM_H
#define ROBOTAUDIOSPECTRUM_H

#include <Core/System/Network/FlowNetwork/skflowsat.h>

#include <UI/FLTK/skfltkui.h>
#include <UI/FLTK/skfltkwindows.h>
#include <UI/FLTK/skfltkhlayout.h>
#include <UI/FLTK/skfltkvlayout.h>
#include <UI/FLTK/skfltkbuttons.h>
#include <UI/FLTK/skfltkmenus.h>
#include <UI/FLTK/skfltkviews.h>
#include <UI/FLTK/skfltkcurvechart.h>
#include <UI/FLTK/skfltkbarchart.h>

class RobotAudioSpectrum extends SkFlowSat
{
    SkTreeMap<SkString, SkVariant> guiIndex;

    SkFltkDoubleWindow *window;
    SkFltkCurvesChart *fft;
    SkFltkComboBox *chansCombo;

    SkString fftChanName;
    SkFlowChanID fftChan;
    bool inputChanEnabled;

    public:
        Constructor(RobotAudioSpectrum, SkFlowSat);

        Slot(fullScreen);
        Slot(selectChannel);

    private:
        void buildUI();

        bool onSetup();
        void onInit();
        void onQuit();

        void onChannelAdded(SkFlowChanID chanID)    override;
        void onChannelRemoved(SkFlowChanID chanID)  override;

        void onFlowDataCome(SkFlowChannelData &chData);

        void onInputSetup(SkFlowChannel *ch);
};

#endif // ROBOTAUDIOSPECTRUM_H
