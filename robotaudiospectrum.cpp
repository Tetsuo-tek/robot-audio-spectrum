#include "robotaudiospectrum.h"
#include "gui.h"

#include <Core/Containers/skarraycast.h>

ConstructorImpl(RobotAudioSpectrum, SkFlowSat)
{
    window = nullptr;
    fft = nullptr;
    chansCombo = nullptr;

    fftChan = -1;
    inputChanEnabled = false;

    setObjectName("RobotAudioSpectrum");

    SlotSet(fullScreen);
    SlotSet(selectChannel);
}
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotAudioSpectrum::buildUI()
{
    SkVariant indexVal;
    indexVal.fromJson(GUI_INDEX);
    indexVal.copyToMap(guiIndex);

    int index = guiIndex["gui/mainwindow.json"].toInt();
    window = DynCast(SkFltkDoubleWindow, ui->loadFromJSON(gui[index].data));
    Attach(window, closed, this, quit, SkQueued);

    fft = DynCast(SkFltkCurvesChart, ui->get("fft"));

    SkFltkLightButton *fsButton = DynCast(SkFltkLightButton, ui->get("fsButton"));
    Attach(fsButton, clicked, this, fullScreen, SkQueued);

    chansCombo = DynCast(SkFltkComboBox, ui->get("chansCombo"));
    Attach(chansCombo, picked, this, selectChannel, SkQueued);

    SkFltkButton *quitButton = DynCast(SkFltkButton, ui->get("quitButton"));
    Attach(quitButton, clicked, this, quit, SkQueued);

    window->show();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotAudioSpectrum::onSetup()
{
    return true;
}

void RobotAudioSpectrum::onInit()
{
    buildUI();
}

void RobotAudioSpectrum::onQuit()
{
    window = nullptr;
    fft = nullptr;
    chansCombo = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotAudioSpectrum::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    if (ch->name == fftChanName)
        onInputSetup(ch);

    //if (ch->flow_t == FT_AUDIO_DATA || ch->flow_t == FT_AUDIO_PREVIEW_DATA)
    if (ch->flow_t == FT_AUDIO_FFT)
    {
        int index = chansCombo->add(ch->name.c_str());

        if (index > -1 && ch->name == fftChanName)
            chansCombo->setCurrentIndex(index);
    }
}

void RobotAudioSpectrum::onChannelRemoved(SkFlowChanID chanID)
{
    if (!chansCombo)
        return;

    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    int index = chansCombo->find(ch->name.c_str());

    if (index > -1)
        chansCombo->remove(index);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotAudioSpectrum::onInputSetup(SkFlowChannel *ch)
{
    SkFlowSync *sync = buildSyncClient();

    SkStringList parsedName;
    ch->name.split(".", parsedName);
    AssertKiller(parsedName.count() < 2);

    /*AssertKiller(!sync->existsOptionalPairDb(parsedName.first().c_str()));
    AssertKiller(!sync->setCurrentDbName(parsedName.first().c_str()));
    SkString chanPropsName(ch->name);
    chanPropsName.append("_CHAN_PROPS");*/

    AssertKiller(!sync->existsOptionalPairDb(ch->name.c_str()));
    AssertKiller(!sync->setCurrentDbName(ch->name.c_str()));

    SkVariant v;
    SkArgsMap paramsMap;
    ULong tickIntervalUS = 0;

    if (sync->getVariable("tickTimePeriod", v))
        tickIntervalUS = v.toFloat()*1000000;

    if (tickIntervalUS)
    {
        if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
        {
            ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
            eventLoop()->changeFastZone(tickIntervalUS);
            eventLoop()->changeSlowZone(tickIntervalUS*4);
            eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);
        }
    }

    sync->close();
    sync->destroyLater();
    //

    fft->clear();

    SkFltkCurve *c = new SkFltkCurve;
    c->enabled = true;
    c->color = SkColor::yellow().toFlColor();
    fft->addCurve(c);

    fftChan = ch->chanID;
    subscribeChannel(ch);

    eventLoop()->changeFastZone(tickIntervalUS);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotAudioSpectrum::onFlowDataCome(SkFlowChannelData &chData)
{
    if (chData.chanID == fftChan)
    {
        float *fftData = SkArrayCast::toFloat(chData.data.toVoid());
        float count = chData.data.size()/sizeof(float);

        float ratio = count / fft->w();

        if (!ratio)
            ratio = 1;

        SkFltkCurve *c = fft->curve(0);
        c->samples.clear();

        for(float i=0; c->samples.count()<static_cast<ULong>(fft->w()); i+=ratio)
            c->samples.append(fftData[(int) i]);

        fft->redraw();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAudioSpectrum, fullScreen)
{
    SilentSlotArgsWarning();

    static bool fullscreen = false;

    if (!fullscreen)
        window->enableFullScreen();

    else
        window->disableFullScreen();

    window->redraw();
    fullscreen = !fullscreen;
}

SlotImpl(RobotAudioSpectrum, selectChannel)
{
    SilentSlotArgsWarning();

    SkFlowChannel *ch = channel(chansCombo->currentText());
    AssertKiller(!ch);

    if (fftChan > -1)
        unsubscribeChannel(fftChan);

    fftChanName = ch->name;
    onInputSetup(ch);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
