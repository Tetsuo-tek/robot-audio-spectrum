#include "robotaudiospectrum.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);
    skApp->init(5000, 150000, SK_TIMEDLOOP_RT, false);
    new RobotAudioSpectrum;
    return skApp->exec();
}
